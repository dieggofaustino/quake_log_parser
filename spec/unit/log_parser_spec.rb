require_relative '../../lib/log_parser'

describe LogParser do
  let(:file) { double(:file) }
  let(:file_lines) do
    [
      '  0:00 InitGame: \sv_floodProtect\1\sv_maxPing\0',
      ' 20:34 ClientUserinfoChanged: 2 n\Isgalamido\t\0',
      '  0:25 ClientUserinfoChanged: 2 n\Dono da Bola\t\0',
      '  0:27 ClientUserinfoChanged: 2 n\Mocinha\t\0',
      '  1:08 Kill: 3 2 6: Isgalamido killed Mocinha by MOD_ROCKET',
      '  1:41 Kill: 1022 2 19: <world> killed Dono da Bola by MOD_FALLING',
      '  1:42------------------------------------------------------------',
      '  0:00 InitGame: \sv_floodProtect\1\sv_maxPing\0',
      '  0:25 ClientUserinfoChanged: 2 n\Dono da Bola\t\0',
      '  0:27 ClientUserinfoChanged: 2 n\Mocinha\t\0',
      '  0:59 ClientUserinfoChanged: 3 n\Isgalamido\t\0',
      '  1:08 ClientUserinfoChanged: 4 n\Zeh\t\0',
      '  1:08 Kill: 3 2 6: Isgalamido killed Mocinha by MOD_ROCKET',
      '  1:26 Kill: 1022 4 22: <world> killed Zeh by MOD_TRIGGER_HURT',
      '  1:41 Kill: 1022 2 19: <world> killed Dono da Bola by MOD_FALLING',
      '  2:11 Kill: 2 4 6: Dono da Bola killed Zeh by MOD_ROCKET',
      '  2:17 ------------------------------------------------------------'
    ]
  end

  subject { described_class.new('log_file_path') }

  describe '#parse' do
    it 'should parse log' do
      allow(File).to receive(:open).with('log_file_path').and_return(file)
      expect(file).to receive(:readlines).and_return(file_lines)

      result = {
        'game_1' => {
          total_kills: 2,
          players: ['Isgalamido', 'Dono da Bola', 'Mocinha'],
          kills: { 'Isgalamido' => 1, 'Dono da Bola' => -1 }
        },
        'game_2' => {
          total_kills: 4,
          players: ['Dono da Bola', 'Mocinha', 'Isgalamido', 'Zeh'],
          kills: { 'Isgalamido' => 1, 'Zeh' => -1, 'Dono da Bola' => 0 }
        }
      }

      expect(subject.parse).to eq result
    end
  end

  describe '#killers_ranking' do
    it 'returns ranking of killers by game' do
      allow(File).to receive(:open).with('log_file_path').and_return(file)
      expect(file).to receive(:readlines).and_return(file_lines)

      result = {
        'game_1' => { 'Isgalamido' => 1, 'Dono da Bola' => -1 },
        'game_2' => { 'Isgalamido' => 1, 'Dono da Bola' => 0, 'Zeh' => -1 }
      }

      expect(subject.killers_ranking).to eq result
    end
  end

  describe '#deaths_ranking' do
    it 'returns ranking of deaths by game' do
      allow(File).to receive(:open).with('log_file_path').and_return(file)
      expect(file).to receive(:readlines).and_return(file_lines)

      result = {
        'game_1' => { 'MOD_ROCKET' => 1, 'MOD_FALLING' => 1 },
        'game_2' => { 'MOD_ROCKET' => 2, 'MOD_TRIGGER_HURT' => 1, 'MOD_FALLING' => 1 }
      }

      expect(subject.deaths_ranking).to eq result
    end
  end
end
