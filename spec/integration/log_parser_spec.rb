require_relative '../../lib/log_parser'

describe LogParser do
  subject { described_class.new('spec/integration/games_test.log') }

  describe '#parse' do
    it 'should parse log' do
      result = {
        'game_1' => {
          total_kills: 0,
          players: ['Isgalamido'],
          kills: {}
        },
        'game_2' => {
          total_kills: 2,
          players: ['Isgalamido', 'Mocinha'],
          kills: { 'Isgalamido' => 0 }
        }, 'game_3' => {
          total_kills: 4,
          players: ['Dono da Bola', 'Mocinha', 'Isgalamido', 'Zeh'],
          kills: { 'Isgalamido' => 1, 'Zeh' => -1, 'Dono da Bola' => 0 }
        }
      }

      expect(subject.parse).to eq result
    end
  end

  describe '#killers_ranking' do
    it 'show ranking game killers' do
      result = {
        'game_1' => {},
        'game_2' => { 'Isgalamido' => 0 },
        'game_3' => { 'Isgalamido' => 1, 'Dono da Bola' => 0, 'Zeh' => -1 }
      }

      expect(subject.killers_ranking).to eq result
    end
  end

  describe '#deaths_ranking' do
    it 'show ranking game deaths' do
      result = {
        'game_1' => {},
        'game_2' => { 'MOD_TRIGGER_HURT' => 1, 'MOD_ROCKET_SPLASH' => 1 },
        'game_3' => { 'MOD_ROCKET' => 2, 'MOD_TRIGGER_HURT' => 1, 'MOD_FALLING' => 1 }
      }

      expect(subject.deaths_ranking).to eq result
    end
  end
end
