# Quake log parser #
O Quake log parser é um script em ruby que lê e extrai as informações dos jogos do servidor de quake 3 arena.

Ele informa quem são os jogadores, a quantidade total de mortes e de assassinatos por jogador em cada jogo. Também mostra as causas de mortes por partida e quantas vezes algém morreu dessa maneira, além de um ranking dos jogadores que mais mataram em cada game.

### Dependências ###
O ruby e rspec devem estar instalados.

### Execução das tarefas ###
Primeiro faça o clone do repositório na máquina local. Logo após entre no diretório da aplicação e execute os comandos abaixo para cada necessidade.

Para mostrar as informações de cada jogo:

`ruby parser.rb parse`

Para mostrar dos jogadores que mais mataram:

`ruby parser.rb killers_ranking`

Para mostrar as causas das mortes por jogo:

`ruby parser.rb deaths_ranking`

### Execução dos testes ###
`rspec spec/`