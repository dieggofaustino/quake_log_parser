class LogParser
  def initialize(log_file_path = 'lib/games.log')
    @log_file_path = log_file_path
  end

  # Mostra os jogadores, a quantidade total de mortes
  # e a quantidade de assassinatos por jogador em cada jogo
  #
  # Retorno: hash
  def parse
    {}.tap do |result|
      game_records do |game, records|
        players = game_players(records)
        total_kills, killers = game_kills(records)

        result["game_#{game}"] = {
          total_kills: total_kills,
          players: players,
          kills: killers
        }
      end
    end
  end

  # Mostra um ranking de assassinato dos jogadores por jogo
  #
  # Retorno: hash
  def killers_ranking
    {}.tap do |result|
      game_records do |game, records|
        _, killers = game_kills(records)
        result["game_#{game}"] = killers.sort_by(&:last).reverse.to_h
      end
    end
  end

  # Mostra a quantidade de mortes de cada jogo agrupada pela causa
  #
  # Retorno: hash
  def deaths_ranking
    {}.tap do |result|
      game_records do |game, records|
        result["game_#{game}"] = game_deaths(records)
      end
    end
  end

  private

  # Separa os jogos dentro do arquivo de log
  #
  # Retorno 1: número do jogo (int)
  # Retorno 2: registros do jogo (array)
  def game_records
    game = 1
    starts = nil
    ends = nil

    file_lines = File.open(log_file_path).readlines

    file_lines.each_with_index do |record, i|
      starts = i if record =~ /InitGame/
      ends = i if starts && record =~ /-----------+/

      if ends
        yield(game, file_lines[starts..ends])

        starts = nil
        ends = nil
        game += 1
      end
    end
  end

  # Extrai os jogadores do jogo
  #
  # Parametros
  # records: linhas do arquivo que compõem um único jogo
  #
  # Retorno: jogadores do jogo (array)
  def game_players(records)
    players = []

    records.each do |record|
      next unless record =~ /ClientUserinfoChanged/
      players << record.split(/ClientUserinfoChanged:\s\d\sn\\/).last.split(/\\t/).first
    end

    players.uniq
  end

  # Contabiliza a quantidade de assassinatos de cada jogador por jogo
  #
  # Parametros
  # records: linhas do arquivo que compõem um único jogo
  #
  # Retorno 1: quantidade total de mortes no jogo (int)
  # Retorno 2: jogadores do jogo que mataram e sua respectiva quantidade de assassinatos (hash)
  def game_kills(records)
    total_kills = 0
    killers = Hash.new { |hash, key| hash[key] = 0 }

    records.each do |record|
      record = record.split(/^\s+\d+:\d{2}\s/).last
      next unless record.start_with?('Kill: ')

      record = record.split(/Kill:\s\d+\s\d+\s\d+:\s/).last
      record = record.split(/\sby\s/).first
      killer, killed = record.split(' killed ')

      total_kills += 1

      if killer == '<world>'
        # Quando o <world> mata o player ele perde -1 kill
        killers[killed] -= 1
      else
        killers[killer] += 1
      end
    end

    return total_kills, killers
  end

  # Contabiliza a quantidade de mortes por determinada forma no jogo
  #
  # Parametros
  # records: linhas do arquivo que compõem um único jogo
  #
  # Retorno: causa da morte e a quantidade de vezes que alguém morreu dessa maneira (hash)
  def game_deaths(records)
    deaths = Hash.new { |hash, key| hash[key] = 0  }

    records.each do |record|
      record = record.split(/^\s+\d+:\d{2}\s/).last
      next unless record.start_with?('Kill: ')

      record = record.split(/Kill:\s\d+\s\d+\s\d+:\s/).last
      death = record.split(/\sby\s/).last.chomp

      deaths[death] += 1
    end

    deaths
  end

  attr_reader :log_file_path
end
